
# Project package count report

Create a `CSV` report for the number of packages per project. Reports for a single group as well as all projects on a self-managed instance are supported.

## Usage

`python3 count_packages.py $GIT_TOKEN`

**Parameters:**

* `$GIT_TOKEN`: Personal Access Token with `api` permissions, able to read all projects you want to report on. For GitLab.com, a top group owner token should be used. For self-managed instances, an instance admin token can be used, or alternatively a non-admin user that has access to all projects.
* `--gitlab`: Optional GitLab instance url, defaults to `https://gitlab.com`
* `--group`: Optional GitLab group path to limit the report on a single group. Not using this parameter will query all projects visibile to the `$GIT_TOKEN` on the instance.

## DISCLAIMER

This script is provided **for educational purposes only**. It is not supported by GitLab. You can create an issue or contribute via MR if you encounter any problems. This project only reads the GraphQL API. The report lists project URLs and package counts, so no actually sensitive information should be leaked by this project.
