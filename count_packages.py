import requests
import argparse
import csv

def request_project_storage_instance(gitlab, token, page, param = ""):
    print("Querying project statistics page %s" % str(page))
    projects = []
    api_url = gitlab + "/api/graphql"
    headers = {'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json'}
    query = """
        query {
          projects %s {
            nodes {
              id
              name
              fullPath
              packages {
                count
              }
            }
            pageInfo {
                endCursor
                hasNextPage
            }
          }
        }""" % param
    # use endcursor, then projects(after: "endcursor")
    try:
        r = requests.post(api_url, json={'query': query}, headers = headers)
        if "data" in r.json():
            return r.json()
        else:
            print("[ERROR] There was a problem with the query")
            print(r.json())
            return {}
    except Exception as e:
        print("[ERROR] Could not retrieve query!")
        print(e)
        return {}

def request_project_storage_group(gitlab, token, group, page, param = ""):
    print("Querying project statistics page %s" % str(page))
    projects = []
    api_url = gitlab + "/api/graphql"
    headers = {'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json'}
    query = """
        query {
          group(fullPath:"%s") {
            projects(includeSubgroups:true %s) {
              nodes {
                id
                name
                fullPath
                packages {
                  count
                }
              }
              pageInfo {
                  endCursor
                  hasNextPage
              }
            }
          }
        }""" % (group, param)
    # use endcursor, then projects(after: "endcursor")
    try:
        r = requests.post(api_url, json={'query': query}, headers = headers)
        if "data" in r.json():
            return r.json()
        else:
            print("[ERROR] There was a problem with the query")
            print(r.json())
            return {}
    except Exception as e:
        print("[ERROR] Could not retrieve query!")
        print(e)
        return {}

parser = argparse.ArgumentParser(description='Get storage of all projects')
parser.add_argument('token', help='GraphQL API token')
parser.add_argument('--gitlab', help='GitLab instance url', default="https://gitlab.com")
parser.add_argument('--group', help='Only fetch projects in a particular group. Full group path string.')
args = parser.parse_args()

gitlab = args.gitlab.strip("/")
has_next_page = True
cursor = ""
projects = []
count = 0

if not args.group:
    while has_next_page:
        count += 1
        query_results = request_project_storage_instance(gitlab, args.token, count, cursor)
        if query_results:
            projects.extend(query_results["data"]["projects"]["nodes"])
            has_next_page = query_results["data"]["projects"]["pageInfo"]["hasNextPage"]
            cursor = '(after: "%s")' % query_results["data"]["projects"]["pageInfo"]["endCursor"]
else:
    while has_next_page:
        count += 1
        query_results = request_project_storage_group(gitlab, args.token, args.group, count, cursor)
        if query_results:
            projects.extend(query_results["data"]["group"]["projects"]["nodes"])
            has_next_page = query_results["data"]["group"]["projects"]["pageInfo"]["hasNextPage"]
            cursor = ',after: "%s"' % query_results["data"]["group"]["projects"]["pageInfo"]["endCursor"]

results = []

for project in projects:
    result = {}
    result["id"] = project["id"][project["id"].rfind("/")+1:]
    result["name"] = project["name"]
    result["url"] = gitlab + "/" + project["fullPath"]
    if project["packages"]:      
      result["package_count"] = str(project["packages"]["count"])
    else:
        result["package_count"] = "0"
    results.append(result)

with open("project_package_counts.csv","w") as outfile:
    writer = csv.writer(outfile, delimiter="\t")
    header = ["id","name","url","package_count"]
    writer.writerow(header)
    for result in results:
        writer.writerow([result[field] for field in header])
